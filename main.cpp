#include <iostream>
#include <stdio.h>
#include <conio.h>
#include <string.h>
#include <stdlib.h>
#include <iomanip>
using namespace std;

//Class Theater
class theater{
  public: 
  char theater_no[100], judul[100], kursi[2][5][10];
  int harga;
  void get_theaterTersedia();
  void kosong();
  void check();
  void get_theaterKursi();
  void posisi(int pos);
  void booking();
  theater();
};
// Instansiasi class theater menjadi object
theater theaterList[10];

// Menjalankan object saat program pertama kali dijalankan
theater::theater(){
 strcpy(theaterList[0].theater_no , "1");
 strcpy(theaterList[0].judul,"Love Live School Idol Project The Movie");
 theaterList[0].harga = 25000;

 strcpy(theaterList[1].theater_no , "2");
 strcpy(theaterList[1].judul,"Digimon Adventure The Movie");
 theaterList[1].harga = 30000;
 
 theaterList[0].kosong();
 theaterList[1].kosong();
}


// Method get list theater yang tersedia
void theater::get_theaterTersedia() {
 system("cls");

 strcpy(theaterList[0].theater_no , "1");
 strcpy(theaterList[0].judul,"Love Live School Idol Project The Movie");
 theaterList[0].harga = 25000;

 strcpy(theaterList[1].theater_no , "2");
 strcpy(theaterList[1].judul,"Digimon Adventure The Movie");
 theaterList[1].harga = 30000;
 
 cout<<"               			"<<endl;
 for(int n=0;n<2;n++){
 	cout<<"-------------------------------------------------------------------"<<endl;
 	cout<<"\t\t\t\tTheater "<<theaterList[n].theater_no<<endl<<endl;
    cout<<"\tJudul Film\t: "<<theaterList[n].judul<<endl;
    cout<<"\tHarga\t\t: "<<theaterList[n].harga<<endl<<endl;
    cout<<"-------------------------------------------------------------------"<<endl;
 }
 
 cout<<"Tekan Enter untuk kembali ke menu utama ";
}


// Method menjadikan semua kursi kosong
void theater::kosong(){
	for(int baris=0;baris<2;baris++){
		for(int kolom=0;kolom<5;kolom++){
			strcpy(theaterList[0].kursi[baris][kolom],"Kosong");
			strcpy(theaterList[1].kursi[baris][kolom],"Kosong");
		}
	}
}

// Method untuk posisi kursi
void theater::posisi(int pos){
	int nomor = 0, sisa = 0;
	
	for(int baris=0;baris<2;baris++){
		cout<<endl;
		for(int kolom=0;kolom<5;kolom++){
			nomor++;
			if(strcmp(theaterList[pos].kursi[baris][kolom],"Kosong")==0){
			  cout.width(5);
	          cout<<nomor<<".";
	          cout.width(10);
	          cout<<theaterList[pos].kursi[baris][kolom];
	          sisa++;	
			}else{
			  cout.width(5);
	          cout<<nomor<<".";
	          cout.width(10);
	          cout<<theaterList[pos].kursi[baris][kolom];
			}
		}
	}
	cout<<endl<<endl<<"Kursi yang tersedia "<<sisa<<endl<<endl;
	
}

// Method untuk melihat jumlah kursi setiap theater
void theater::get_theaterKursi(){
	system("cls");
	int n;
	char no_theater[5];
	cout<<"Masukkan nomor theater : ";
	cin>>no_theater;
	
	for(n=0;n<2;n++){
		if(strcmp(theaterList[n].theater_no, no_theater)==0)
		break;
	}
	
	while(n<=2){
		int a = 0;
		 cout<<"--------------------------------------------------------------------------------------"<<endl;
		 cout<<"                		        THEATER "<<no_theater<<"                         	  "<<endl;
		 cout<<"--------------------------------------------------------------------------------------"<<endl;
		theaterList[0].posisi(n);
		
		for(int baris=0;baris<2;baris++){
			for(int kolom=0;kolom<5;kolom++){
				a++;
				if(strcmp(theaterList[n].kursi[baris][kolom],"Kosong")!=0)
				cout<<endl<<"Kursi nomor "<<a<<" sudah di booking atas nama "<<theaterList[n].kursi[baris][kolom]<<".";
			}
		}
		break;
	}
	
//	if(n>2){
//		cout<<"Masukkan Nomor Theater yang benar: ";
//	}
}

void theater::booking(){
	int no_kursi,n,bayar,kembali;
	char no_theater[5];
	system("cls");
	cout<<"Pilih nomor theater : ";
	cin>>no_theater;
	
	for(n=0;n<2;n++){
		if(strcmp(theaterList[n].theater_no, no_theater)==0)
		break;
	}
	
	while(n<=2){
		cout<<endl<<"Pilih nomor kursi : ";
		cin>>no_kursi;
		
		if(no_kursi>10){
			cout<<"Dalam theater ini hanya ada 10 kursi"<<endl;
		}else{
			if(strcmp(theaterList[n].kursi[no_kursi/5][(no_kursi%5)-1],"Kosong")==0){
				cout<<"Masukkan Nama Anda : ";
				cin>>theaterList[n].kursi[no_kursi/5][(no_kursi%5)-1];
				cout<<endl<<endl<<"Harga : Rp."<<theaterList[n].harga<<endl;
				cout<<"Bayar : Rp.";
				cin>>bayar;
				cout<<endl<<"Kembali : Rp."<<bayar-theaterList[n].harga;
				cout<<endl<<endl<<"Kursi Berhasil di booking"<<endl<<endl;
				break;
			}else{
				cout<<"Kursi sudah terbooking"<<endl<<endl;
			}
		}
	}
}

// Menu Utama
int main(){
	theater t;
	do{
	 system("cls");
	 int pilih;
	 cout<<"----------------------------------------------------"<<endl;
	 cout<<"                UHAMKA CINEMA                       "<<endl;
	 cout<<"----------------------------------------------------"<<endl<<endl;

	 cout<<"1. Film Sedang Tayang"<<endl;
	 cout<<"2. Lihat Ketersediaan Kursi"<<endl;
	 cout<<"3. Booking"<<endl;
	 cout<<"4. Exit"<<endl;
	 cout<<"Masukkan Pilihan : ";
	 cin>>pilih;
	 switch(pilih){
	 	case 1 :
	 		theaterList[0].get_theaterTersedia();
	 	break;
	 	case 2 :
	 		theaterList[0].get_theaterKursi();
	 	break;
	 	case 3 :
	 		theaterList[0].booking();
	 	break;
	 	case 4:
	 		exit(0);
	 }
	}while(getch());
return 0;
}
